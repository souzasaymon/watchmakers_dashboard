import React from 'react';

import MainTemplate from '../templates/MainTemplate';

function About() {
  return (
    <MainTemplate>
      This is the about page.
    </MainTemplate>
  );
}

export default About;
