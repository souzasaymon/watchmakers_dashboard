import React, { useState, useEffect } from 'react';

import MainTemplate from '../templates/MainTemplate';
import HeaderMenu from '../molecules/HeaderMenu';
import Anchor from '../atoms/Anchor';

import isMobile from '../utils/isMobile';

import {
  HomePageContainer,
  BannerSection,
  Title,
  PromotionSection,
  PromotionItem,
} from './styles/index.style';

function Home() {
  const [deviceIsMobile, setdeviceIsMobile] = useState(false);

  useEffect(() => {
    setdeviceIsMobile(isMobile());
  });

  return (
    <MainTemplate>
      <HomePageContainer>
        {
          deviceIsMobile ? (
            <p>teste</p>
          ) : (
            <HeaderMenu />
          )
        }
        <BannerSection>
          <Title>Welcome to the Watchmakers&apos; Union of Manaus!</Title>
        </BannerSection>
        <PromotionSection>
          <PromotionItem>
            <Anchor href="/about" text="Vá para a próxima página" />
          </PromotionItem>
          <PromotionItem>
            <Anchor href="/login" text="Vá para o login" />
          </PromotionItem>
        </PromotionSection>
      </HomePageContainer>
    </MainTemplate>
  );
}

export default Home;
