import styled from 'styled-components';

const HomePageContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

const BannerSection = styled.div`
  background-color: black;
  text-align: center;
`;

const Title = styled.h1`
  color: white;
  padding-top: 24px;
  padding-bottom: 24px;

  @media only screen and (min-width: 992px) {
    padding-top: 96px;
    padding-bottom: 96px;
  } 
`;

const PromotionSection = styled.div`
  display: flex;
  flex-flow: row wrap;
`;

const PromotionItem = styled.div`
  width: 100%;

  @media only screen and (min-width: 992px) {
    width: 50%;
  } 
`;

export {
  HomePageContainer,
  BannerSection,
  Title,
  PromotionSection,
  PromotionItem,
};

