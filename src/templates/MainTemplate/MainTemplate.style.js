import styled, { createGlobalStyle } from 'styled-components';

const GlobalStyle = createGlobalStyle`
  body {
    margin: 0;
  }
`;

const BaseStyle = styled.div`
  min-height: 100vh;
  font-family: Poppins;
`;

export {
  BaseStyle,
  GlobalStyle,
};
