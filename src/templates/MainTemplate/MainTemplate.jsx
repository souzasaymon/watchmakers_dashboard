import React, { useEffect } from 'react';
import { node } from 'prop-types';

import Fonts from '../../utils/fonts';

import {
  BaseStyle,
  GlobalStyle,
} from './MainTemplate.style';

function MainTemplate(props) {
  const { children } = props;

  useEffect(() => {
    Fonts();
  }, []);

  return (
    <>
      <GlobalStyle />
      <BaseStyle>
        { children }
      </BaseStyle>
    </>
  );
}

MainTemplate.propTypes = {
  children: node,
};

MainTemplate.defaultProps = {
  children: null,
};

export default MainTemplate;
