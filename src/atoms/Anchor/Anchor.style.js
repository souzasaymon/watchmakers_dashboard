import styled from 'styled-components';

const BaseStyle = styled.a`
  :hover {
    color: white;
    cursor: pointer;
  }
`;

export { BaseStyle };
