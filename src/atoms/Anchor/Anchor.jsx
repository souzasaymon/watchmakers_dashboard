import React from 'react';
import Link from 'next/link';
import { string } from 'prop-types';

import {
  BaseStyle,
} from './Anchor.style';

function Anchor(props) {
  const { text, href } = props;

  return (
    <Link href={href}>
      <BaseStyle>
        {text}
      </BaseStyle>
    </Link>
  );
}

Anchor.propTypes = {
  text: string.isRequired,
  href: string.isRequired,
};

export default Anchor;
