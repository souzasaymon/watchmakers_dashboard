import React from 'react';

import Anchor from '../../atoms/Anchor';

import {
  MenuContainer,
  MenuContent,
  MenuItem,
} from './HeaderMenu.style';

function HeaderMenu() {
  return (
    <MenuContainer>
      <MenuContent>
        <MenuItem>
          <Anchor href="/services" text="Our services" />
        </MenuItem>
        <MenuItem>
          <Anchor href="/about" text="About us" />
        </MenuItem>
        <MenuItem>
          <Anchor href="/about" text="Login" />
        </MenuItem>
      </MenuContent>
    </MenuContainer>
  );
}

export default HeaderMenu;
