import styled from 'styled-components';

const MenuContainer = styled.div`
  display: flex;
  justify-content: flex-end;
  background-color: DeepSkyBlue;
`;

const MenuContent = styled.div`
  display: flex;
  justify-content: space-around;
  margin-top: 16px;
  margin-bottom: 16px;
`;

const MenuItem = styled.div`
  margin-left: 8px;
  margin-right: 8px;
`;

export {
  MenuContainer,
  MenuContent,
  MenuItem,
};
