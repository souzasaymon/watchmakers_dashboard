# watchmakers_dashboard

[![React](https://img.shields.io/badge/React%20Js-16.8.6-blue.svg)](https://github.com/facebook/react/releases/tag/v16.8.1)
[![NextJS](https://img.shields.io/badge/Next%20Js-8.1.0-black.svg)](https://github.com/zeit/next.js)
[![Styled Components](https://img.shields.io/badge/Styled%20Components-4.2.0-purple.svg)](https://www.styled-components.com/releases#v4.1.2)
[![Node](https://img.shields.io/badge/Node-10.15.1-darkgreen.svg)](https://nodejs.org/en/)
[![NPM](https://img.shields.io/badge/NPM-6.8.0-green.svg)]()

_A web page for the Watchmakers' Union of Manaus, Amazon, Brazil_

> Installation and use
1. Clone the repository
2. Open a terminal inside the repository
3. Run `npm run build`
4. Run `npm run dev`
5. Open a browser tab and add the url http://localhost:3000/